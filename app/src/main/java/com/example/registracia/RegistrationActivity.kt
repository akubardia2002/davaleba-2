package com.example.registracia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegistrationActivity : AppCompatActivity() {
    private lateinit var editTextEmail : EditText
    private lateinit var editTextPassword : EditText
    private lateinit var editTextRepeatPassword : EditText
    private lateinit var registrationButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
    init()
        registrationListeners()
    }


    private fun init(){
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextRepeatPassword = findViewById(R.id.editTextRepeatPassword)
        registrationButton = findViewById(R.id.registrationButton)

    }
    private fun registrationListeners(){
        registrationButton.setOnClickListener {

                val password = editTextPassword.text.toString()
                val repeatPassword = editTextRepeatPassword.text.toString()
                val email = editTextEmail.text.toString()
                if(password.isNotEmpty() and repeatPassword.isNotEmpty() and email.isNotEmpty()){
                if (password.equals(repeatPassword)) {

                    FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                        if(task.isSuccessful) {
                            Toast.makeText(this, "Registration Successfully Completed!", Toast.LENGTH_SHORT).show()
                        }else{
                            Toast.makeText(this, "The E-mail You Entered Is Incorrect!", Toast.LENGTH_SHORT).show()
                        }
                    }
                } else {
                    Toast.makeText(
                        this,
                        "Error! Entered Passwords Do Not Match!",
                        Toast.LENGTH_SHORT
                    ).show() }}
            else{Toast.makeText(this, "You Must Enter A Value For All Of The Fields", Toast.LENGTH_SHORT).show()}
        }}
    }
